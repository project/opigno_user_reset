Opigno User Reset
================================================================================

The Opigno User Reset module allows admins to use a "reset" operation to clear 
all of a users quiz, course, and class progress. This can be useful when testing, 
developing a course, or in special situations when running an LMS. The operation 
should be used carefully as no attempt is made to preserve data for rollback.


Installation
--------------------------------------------------------------------------------

Do the usual download and place in the `modules` directory process, or if you
are using composer: `$ composer require 'drupal/opigno_user_reset'`.

When the code is ready, use Drush or the UI to install the module as usual.


Configuration
--------------------------------------------------------------------------------

The only required configuration is to ensure that the Quiz module is set to 
remove content when users are deleted; navigate to: `/admin/quiz/settings/config`
and check the box next to "Delete results when a user is deleted."


Maintainers
--------------------------------------------------------------------------------

Current maintainers:

* Tom Kiehne (tkiehne) - https://www.drupal.org/u/tkiehne


Issues
--------------------------------------------------------------------------------

If you have issues with this module you are welcome to submit issues at
https://www.drupal.org/project/opigno_user_reset (all submitted issues are public).
