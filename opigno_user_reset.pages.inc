<?php

/**
 * @file
 * Page callback file for the Opigno user reset module.
 */

/**
 * Form builder; confirm form for resetting user progress.
 *
 * @ingroup forms
 */
function user_reset_progress_confirm_form($form, &$form_state, $account) {
  global $user;

  $form['_account'] = array('#type' => 'value', '#value' => $account);

  // Prepare confirmation form page title and description.
  if ($account->uid == $user->uid) {
    $question = t('Are you sure you want to reset your class progress?');
  }
  else {
    $question = t('Are you sure you want to reset class progress for %name?', array('%name' => $account->name));
  }

  $description = 'All course, class, and quiz results will be erased for this user.';

  $form['uid'] = array('#type' => 'value', '#value' => $account->uid);
  return confirm_form($form,
    $question,
    'admin/people',
    $description . ' ' . t('This action cannot be undone.'),
    t('Reset user progress'), t('Cancel'));
}

/**
 * Submit handler for the progress reset confirm form.
 *
 * @see user_reset_progress_confirm_form()
 */
function user_reset_progress_confirm_form_submit($form, &$form_state) {
  global $user;
  $account = $form_state['values']['_account'];
  
  // reset account immediately, if the current user has administrative
  // privileges
  if (user_access('clear user progress')) {
    module_invoke('opigno_quiz_app', 'user_delete', $account); 
    module_invoke('quiz', 'user_cancel', $form_state['values'], $account, 'user_progress_reset'); // note: this requires a quiz config setting to work
    _user_reset_progress_delete_users_statistics($account->uid);
  }

  $form_state['redirect'] = 'admin/people';
}


/**
 * Deletes all statistics associated with a given user.
 *
 * @param int $uid
 *  The users id
 */
function _user_reset_progress_delete_users_statistics($uid) {
  $res = db_query("SELECT opigno_statistics_user_course_pk FROM {opigno_statistics_user_course} WHERE uid = :uid", array(':uid' => $uid));
  $sids = array();
  while ($sid = $res->fetchField()) {
    $sids[] = $sid;
  }
  
  db_delete('opigno_statistics_user_course_details')
    ->condition('opigno_statistics_user_course_fk', $sids, 'IN')
    ->execute();

  db_delete('opigno_statistics_user_course')
    ->condition('uid', $uid)
    ->execute();

  db_delete('opigno_statistics_user')
    ->condition('uid', $uid)
    ->execute();

  db_delete('opigno_statistics_user_group')
    ->condition('uid', $uid)
    ->execute();

  // Kill caches
  $bin = 'cache';
  cache_clear_all('opigno_statistics_app_query_user_courses_results:' . $uid, $bin);
  cache_clear_all('opigno_statistics_app_query_user_total_number_of_page_view:' . $uid . '*', $bin, TRUE);

  // TODO: decrement rollup stats (e.g.: number passed a course)?
}